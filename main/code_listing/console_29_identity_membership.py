y = [1,2,3,4,5]
x = 5
print(u'x in y = {}'.format(x in y))
print(u'x not in y = {}'.format(x not in y))

x = 7
print(u'x in y = {}'.format(x in y))
print(u'x not in y = {}'.format(x not in y))
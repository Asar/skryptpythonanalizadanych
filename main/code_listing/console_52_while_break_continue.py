licznik = 0

while licznik < 30:
    print(licznik)
    licznik += 1
    if licznik > 3:
        break
print("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+")

licznik = 0

while  licznik < 10:
    if licznik % 2 == 0:
        licznik += 1
        continue
    print(licznik)  
    licznik += 1
print("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+")

for x in range(1,10):
    if x % 2 == 0:
        continue
    print(x)

print("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+")

for x in range(1,10):
    if x > 5:
        break
    print(x)
class Foo(object):
    def __init__(self, name:str):
        self.__name = name
        
        
fo1 = Foo("name1")
fo2 = fo1

print(u'fo1 is fo2 = {}'.format(fo1 is fo2))
print(u'fo1 is not fo2 = {}'.format(fo1 is not fo2))

fo2 = Foo("name2")
print(u'fo1 is fo2 = {}'.format(fo1 is fo2))
print(u'fo1 is not fo2 = {}'.format(fo1 is not fo2))
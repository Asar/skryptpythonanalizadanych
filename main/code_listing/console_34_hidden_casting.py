x = 5
y = 13.7

print(type(x))
print(type(y))

print('x + y = {}; type(x + y) = {}'.format(x + y, type(x + y)))
print('x - y = {}; type(x - y) = {}'.format(x - y, type(x - y)))
print('x * y = {}; type(x * y) = {}'.format(x * y, type(x * y)))
print('x / y = {}; type(x / y) = {}'.format(x / y, type(x / y)))
print('x // y = {}; type(x // y) = {}'.format(x // y, type(x // y)))
print('x % y = {}; type(x % y) = {}'.format(x % y, type(x % y)))
print('x ** y = {}; type(x ** y) = {}'.format(x ** y, type(x **y)))
x = 1
print(type(x))
x = 1.2
print(type(x))
x = 3+5j
print(type(x))
x = "Python"
print(type(x))
x = [1,2,3]
print(type(x))
x = (1,2,3)
print(type(x))
x = {'a':1, "b":2, 'c':3}
print(type(x))
x = {1, 2, 3}
print(type(x))
x = True
print(type(x))
def foo(throwIron):
    if throwIron is True:
        raise Exception("Throw Iron")
    print("foo")
    
try:
    foo(False)
    print("after foo")
except Exception as e:
    print("error: {}".format(e))
else:
    print("nothing happend")
    
print("next step 1")
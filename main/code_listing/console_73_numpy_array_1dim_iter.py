import numpy

a = numpy.arange(10)**4
print(a)
print(a[4:-2])
a[0:3] = 42
print(a)

for x in a:
    print(x)
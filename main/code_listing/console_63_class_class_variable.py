class Foo(object):
    k = "default_k"
    def __init__(self):
        pass

foo1 = Foo()
foo2 = Foo()
print(foo1.k)
print(foo2.k)
Foo.k = "new_k"
print(foo1.k)
print(foo2.k)
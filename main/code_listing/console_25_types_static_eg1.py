class Foo:
    def __init__(self, text):
        self.t = text
    def toUpper(self):
        return self.t.upper()

f01 = Foo(text="Python")
print(f01.toUpper())

f02 = Foo(text=123)
print(f02.toUpper())
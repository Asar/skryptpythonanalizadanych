class Foo(object):
    def __init__(self, param1, param2):
        self.__a = param1,
        self.__b = param2
        self.i = param1 + " " + param2
    def boo(self):
        return self.__a
    
foo = Foo("aa", "bb")
print(foo.i)
print(foo.boo())

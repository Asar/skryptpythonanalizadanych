import numpy

a = numpy.arange(20).reshape(5,4)
print(type(a))
print(a)

print('shape: {}'.format(a.shape))
print('ndim: {}'.format(a.ndim))
print('dtype: {}'.format(a.dtype))
print('itemsize: {}'.format(a.itemsize))
print('size: {}'.format(a.size))
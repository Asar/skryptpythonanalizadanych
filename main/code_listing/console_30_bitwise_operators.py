x = 1
y = 0

print('x = {:08b}'.format(x))
print('y = {:08b}'.format(y))
print('x & y = {:08b}'.format(x & y))
print('x & x = {:08b}'.format(x & x))
print('x | y = {:08b}'.format(x | y))
print('x | x = {:08b}'.format(x | x))
print('x ^ y = {:08b}'.format(x ^ y))
print('x ^ x = {:08b}'.format(x ^ x))
print('~x = {:08b}'.format(~x))
print('~y = {:08b}'.format(~y))
print('x >> 2 = {:08b}'.format( x >> 2))
print('x << 2 = {:08b}'.format( x << 2))
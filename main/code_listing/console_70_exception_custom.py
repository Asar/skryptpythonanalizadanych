class MyException(Exception):
    def __init__(self, message, param1):
        super().__init__(message)
        self._param1 = param1

def foo(throwIron):
    if throwIron is True:
        raise MyException("Throw Iron", "my param1")
    print("foo")
    
try:
    foo(True)
    print("after foo")
except MyException as e:
    print("error: {}, {}".format(e, e._param1))
    
print("next step 1")
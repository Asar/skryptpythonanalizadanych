x = 5.5 + 23j
y = 13.7 + 73j

print('x + y = {}'.format(x + y))
print('x - y = {}'.format(x - y))
print('x * y = {}'.format(x * y))
print('x / y = {}'.format(x / y))
print('x ** y = {}'.format(x ** y))
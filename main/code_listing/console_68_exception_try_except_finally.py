def foo(throwIron):
    if throwIron is True:
        raise Exception("Throw Iron")
    print("foo")
    
try:
    foo(False)
    print("after foo")
except Exception as e:
    print("error: {}".format(e))

    
print("next step 1")

try:
    foo(True)
    print("after foo")
except Exception as e:
    print("error: {}".format(e))
finally:
    print("The 'try except' is finished")
    
print("next step 2")
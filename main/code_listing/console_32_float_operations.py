x = 5.5
y = 13.7

print('x + y = {}'.format(x + y))
print('x - y = {}'.format(x - y))
print('x * y = {}'.format(x * y))
print('x / y = {}'.format(x / y))
print('x // y = {}'.format(x // y))
print('x % y = {}'.format(x % y))
print('x ** y = {}'.format(x ** y))
x = 5
y = 13

print('x + y = {}'.format(x + y))
print('x - y = {}'.format(x - y))
print('x * y = {}'.format(x * y))
print('x / y = {}'.format(x / y))
print('x // y = {}'.format(x // y))
print('x % y = {}'.format(x % y))
print('x ** y = {}'.format(x ** y))
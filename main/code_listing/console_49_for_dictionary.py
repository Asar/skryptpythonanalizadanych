my_dict = {"a": 1,
    "b": 2,
    "c": 3
}

for k,v in my_dict.items():
    print("kay = {}, val={}".format(k,v))
    print(v ** 2)
print("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+")
for k in my_dict.keys():
    print("kay = {}".format(k))
print("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+")   
for v in my_dict.values():
    print("val={}".format(v))
    print(v ** 2)
x = 5
y = 7

if x < y:
    print("x < y = True")
    
if x > y:
    print("x > y = True")
    
if x:
    print("x = True")
    
if 0 :
    print("0")

if x or y:
    print("x or y = True")
    
if x and y:
    print("x and y = True")
    
if "bc" in "abcd":
    print('"bc" in "abcd"')
    
if 'fizz' in ['bazz', 'fizz', 'fuzz', 'fazz']:
    print("'fizz' in ['bazz', 'fizz', 'fuzz', 'fazz']")
print("<><><>after if-statement")
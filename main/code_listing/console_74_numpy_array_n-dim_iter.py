import numpy

def builder_fun(x,y,z):
    return 100 * x + 10 * y + z
    
a = numpy.fromfunction(builder_fun, (4,4,4), dtype=int)
print(a)

print(a[1,1,1])
print(a[:,:,1])